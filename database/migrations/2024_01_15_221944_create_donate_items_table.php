<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('donate_items', function (Blueprint $table) {
            $table->id();

            $table->string('display_name');
            $table->double('price')->default(0.00);
            $table->string('image_url');
            $table->string('item_description');
            $table->string('item_id_onServer');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('donate_items');
    }
};
