<?php

namespace App\Http\Controllers;

use App\Models\Server;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Ping\Ping;

class MinecraftServerController extends Controller
{
    public function getServers(Request $request) : JsonResponse
    {
        $servers = [];

        foreach(Server::all() as $server)
        {
            try{
                $serverInfo = (new Ping($server->server_ip, $server->port))->getInfos();
                $servers[] = [
                    'slots' => $serverInfo->getMaxPlayers(),
                    'currentPlayerCount' => $serverInfo->getPlayersCount(),
                    'serverName' => $server->display_name,
                    'serverIp' => "$server->server_ip:$server->port",
                    'displayableIP' => $server->displayableIP,
                    'online' => true
                ];
            } catch (\Exception $e){
                $servers[] = [
                    'slots' => 0,
                    'currentPlayerCount' => 0,
                    'serverName' => $server->display_name . " | OFFLINE",
                    'serverIp' => "$server->server_ip:$server->port",
                    'displayableIP' => $server->displayableIP,
                    'online' => false
                ];
            }
        }

        return response()->json($servers);
    }
}
