<?php

namespace App\Http\Controllers;

use App\Models\DonateItem;
use Illuminate\Http\Request;

class DonateItemsController extends Controller
{
    public function getItems(Request $request)
    {
        $items = [];

        foreach(DonateItem::all() as $donateItem)
        {
            $items[] = [
                'name' => $donateItem->display_name,
                'price' => $donateItem->price,
                'image' => $donateItem->image_url,
                'description' => $donateItem->item_description
            ];
        }

        return response()->json($items);
    }
}
